use itertools::Itertools;
use rand::seq::SliceRandom;
use std::cmp::{max, Ordering};
use std::fmt::{Display, Formatter};
use std::iter::Filter;
use std::slice::Iter;
use std::{fmt, io};

macro_rules! parse_input {
    ($x:expr, $t:ident) => {
        $x.trim().parse::<$t>().unwrap()
    };
}

fn random_percent(percent: u8) -> bool {
    let r = rand::random::<u8>() % 100 + 1;
    r <= percent
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Action {
    WAIT,
    COMPLETE(usize),
    SEED(usize, usize),
    GROW(usize),
}

impl PartialOrd for Action {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Action::SEED(from1, to1), Action::SEED(from2, to2)) => {
                //try to get to the center. For the moment, smaller is closer to the center
                Some(to1.cmp(to2))
            }
            (_, _) => None,
        }
    }
}

impl From<String> for Action {
    fn from(str: String) -> Self {
        let words = str.split(" ").collect::<Vec<_>>();
        match words[0] {
            "WAIT" => Action::WAIT,
            "GROW" => Action::GROW(words[1].parse::<usize>().unwrap()),
            "SEED" => Action::SEED(
                words[1].parse::<usize>().unwrap(),
                words[2].parse::<usize>().unwrap(),
            ),
            "COMPLETE" => Action::COMPLETE(words[1].parse::<usize>().unwrap()),
            _ => {
                panic!("Unknown action {}", str);
            }
        }
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Action::WAIT => write!(f, "WAIT"),
            Action::COMPLETE(i) => write!(f, "COMPLETE {}", i),
            Action::GROW(i) => write!(f, "GROW {}", i),
            Action::SEED(i, j) => write!(f, "SEED {} {}", i, j),
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Tree {
    owner_idx: usize,
    size: u8,
    is_dormant: bool,
    square_idx: usize,
}

#[derive(Debug, Eq, PartialEq)]
struct Square {
    index: usize,
    richness: u16,
    neighbours: Vec<i16>,
    coord: (i16, i16, i16), // cube coordinates : https://www.redblobgames.com/grids/hexagons/#coordinates
}

impl Clone for Square {
    fn clone(&self) -> Self {
        Square {
            index: self.index,
            richness: self.richness,
            neighbours: self.neighbours.clone(),
            coord: self.coord,
        }
    }
}

impl Square {
    fn is_neighbour(&self, square_id: i16) -> bool {
        self.neighbours.contains(&square_id)
    }

    fn distance(&self, (x1, y1, z1): (i16, i16, i16)) -> usize {
        let (x, y, z) = self.coord;
        (((x - x1).abs() + (y - y1).abs() + (z - z1).abs()) / 2) as usize
    }

    fn is_aligned(&self, (x1, y1, z1): (i16, i16, i16)) -> bool {
        let (x, y, z) = self.coord;
        x == x1 || y == y1 || z == z1
    }

    fn coord(index: usize) -> (i16, i16, i16) {
        match index {
            0 => (0, 0, 0),
            1 => (1, -1, 0),
            2 => (1, 0, -1),
            3 => (0, 1, -1),
            4 => (-1, 1, 0),
            5 => (-1, 0, 1),
            6 => (0, -1, 1),
            7 => (2, -2, 0),
            8 => (2, -1, -1),
            9 => (2, 0, -2),
            10 => (1, 1, -2),
            11 => (0, 2, -2),
            12 => (-1, 2, -1),
            13 => (-2, 2, 0),
            14 => (-2, 1, 1),
            15 => (-2, 0, 2),
            16 => (-1, -1, 2),
            17 => (0, -2, 2),
            18 => (1, -2, 1),
            19 => (3, -3, 0),
            20 => (3, -2, -1),
            21 => (3, -1, -2),
            22 => (3, 0, -3),
            23 => (2, 1, -3),
            24 => (1, 2, -3),
            25 => (0, 3, -3),
            26 => (-1, 3, -2),
            27 => (-2, 3, -1),
            28 => (-3, 3, 0),
            29 => (-3, 2, 1),
            30 => (-3, 1, 2),
            31 => (-3, 0, 3),
            32 => (-2, -1, 3),
            33 => (-1, -2, 3),
            34 => (0, -3, 3),
            35 => (1, -3, 2),
            36 => (2, -3, 1),
            z => panic!("no square {}", z),
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct Player {
    index: usize,
    sun: u16,
    score: u16,
    is_dormant: bool,
}

#[derive(Debug, Eq, PartialEq)]
struct Board {
    day: usize,
    me: Player,
    opponent: Player,
    trees: Vec<Tree>,
    nutrients: u16,
    squares: Vec<Square>,
}
impl Clone for Board {
    fn clone(&self) -> Self {
        Board {
            day: self.day,
            me: self.me,
            opponent: self.opponent,
            trees: self.trees.clone(),
            nutrients: self.nutrients,
            squares: self.squares.clone(),
        }
    }
}

impl Board {
    fn my_trees(&self) -> Vec<Tree> {
        return self
            .trees
            .iter()
            .filter(|t| t.owner_idx == self.me.index)
            .map(|t| *t)
            .collect_vec();
    }

    fn opponent_trees(&self) -> Vec<Tree> {
        return self
            .trees
            .iter()
            .filter(|t| t.owner_idx == self.opponent.index)
            .map(|t| *t)
            .collect_vec();
    }

    fn get_tree(&self, pos: usize) -> Option<Tree> {
        for t in self.my_trees().iter() {
            if t.square_idx == pos {
                return Some(*t);
            }
        }
        return None;
    }

    fn get_square(&self, id: usize) -> Square {
        for s in self.squares.iter() {
            if s.index == id {
                return s.clone();
            }
        }
        panic!("no square {}", id)
    }

    fn has_tree(&self, square_idx: usize) -> bool {
        for t in self.trees.iter() {
            if t.square_idx == square_idx {
                return true;
            }
        }
        return false;
    }

    fn play_move(&self, my_move: Action, opponent_move: Action) -> Board {
        let mut next = self.clone();

        next
    }
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fn main() {
    let mut input_line = String::new();
    io::stdin().read_line(&mut input_line).unwrap();
    let number_of_cells = parse_input!(input_line, usize); // 37
    let mut squares = vec![];
    {
        for i in 0..number_of_cells as usize {
            let mut input_line = String::new();
            io::stdin().read_line(&mut input_line).unwrap();
            //eprintln!("{}", input_line);
            let inputs = input_line.split(" ").collect::<Vec<_>>();
            let index = parse_input!(inputs[0], usize); // 0 is the center cell, the next cells spiral outwards
            let richness = parse_input!(inputs[1], u16); // 0 if the cell is unusable, 1-3 for usable cells
            let neigh_0 = parse_input!(inputs[2], i16); // the index of the neighbouring cell for each direction
            let neigh_1 = parse_input!(inputs[3], i16);
            let neigh_2 = parse_input!(inputs[4], i16);
            let neigh_3 = parse_input!(inputs[5], i16);
            let neigh_4 = parse_input!(inputs[6], i16);
            let neigh_5 = parse_input!(inputs[7], i16);
            squares.push(Square {
                index,
                richness,
                neighbours: vec![neigh_0, neigh_1, neigh_2, neigh_3, neigh_4, neigh_5],
                coord: Square::coord(index),
            });
        }
    }

    // game loop
    loop {
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let day = parse_input!(input_line, usize); // the game lasts 24 days: 0-23
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let nutrients = parse_input!(input_line, u16); // the base score you gain from the next COMPLETE action
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let inputs = input_line.split(" ").collect::<Vec<_>>();
        let sun = parse_input!(inputs[0], u16); // your sun points
        let score = parse_input!(inputs[1], u16); // your current score
        let player_me = Player {
            index: 0,
            is_dormant: false,
            score,
            sun,
        };
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let inputs = input_line.split(" ").collect::<Vec<_>>();
        let opp_sun = parse_input!(inputs[0], u16); // opponent's sun points
        let opp_score = parse_input!(inputs[1], u16); // opponent's score
        let opp_is_waiting = parse_input!(inputs[2], i32); // whether your opponent is asleep until the next day
        let player_opp = Player {
            index: 1,
            is_dormant: opp_is_waiting == 1,
            score: opp_score,
            sun: opp_sun,
        };
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let number_of_trees = parse_input!(input_line, i32); // the current amount of trees
        let mut trees = vec![];
        for i in 0..number_of_trees as usize {
            let mut input_line = String::new();
            io::stdin().read_line(&mut input_line).unwrap();
            let inputs = input_line.split(" ").collect::<Vec<_>>();
            let cell_index = parse_input!(inputs[0], usize); // location of this tree
            let size = parse_input!(inputs[1], u8); // size of this tree: 0-3
            let is_mine = parse_input!(inputs[2], i32); // 1 if this is your tree
            let is_dormant = parse_input!(inputs[3], i32); // 1 if this tree is dormant
            trees.push(Tree {
                owner_idx: if is_mine == 1 { 0 } else { 1 },
                size,
                is_dormant: is_dormant == 1,
                square_idx: cell_index,
            })
        }
        let mut board = Board {
            day,
            me: player_me,
            opponent: player_opp,
            nutrients,
            trees,
            squares: squares.iter().map(|s| s.clone()).collect_vec(),
        };
        let mut input_line = String::new();
        io::stdin().read_line(&mut input_line).unwrap();
        let number_of_possible_moves = parse_input!(input_line, i32);
        let mut possible_moves = vec![];
        for i in 0..number_of_possible_moves as usize {
            let mut input_line = String::new();
            io::stdin().read_line(&mut input_line).unwrap();
            let possible_move = input_line.trim_matches('\n').to_string();
            if possible_move != "WAIT" {
                possible_moves.push(Action::from(possible_move));
            }
        }
        let ia = BasicIA {
            moves: possible_moves,
            turn: day as usize,
            total_turn: 24,
            board: &board,
        };

        let choosen_action = ia.best_move();
        // Write an action using println!("message...");
        // To debug: eprintln!("Debug message...");

        // GROW cellIdx | SEED sourceIdx targetIdx | COMPLETE cellIdx | WAIT <message>
        println!("{}", choosen_action);
    } // end main loop
}
struct Infos {
    my_tree0_len: usize,
    my_tree1_len: usize,
    my_tree2_len: usize,
    my_tree3_len: usize,
    seed_cost: usize,
    grow1_cost: usize,
    grow2_cost: usize,
    grow3_cost: usize,
    opponent_trees3_count: usize,
}

impl Infos {
    fn new() -> Self {
        Infos {
            my_tree0_len: 0,
            my_tree1_len: 0,
            my_tree2_len: 0,
            my_tree3_len: 0,
            seed_cost: 0,
            grow1_cost: 0,
            grow2_cost: 0,
            grow3_cost: 0,
            opponent_trees3_count: 0,
        }
    }
}

struct BasicIA<'a> {
    moves: Vec<Action>,
    turn: usize,
    total_turn: usize,
    board: &'a Board,
}

impl<'a> BasicIA<'a> {
    // pistes :
    // pas de graines à partir d'arbres lvl 1
    // graines lointaines
    // évaluer le rapport graine/arbre pour voir si il faut planter
    // pas trop de graines par tour

    fn gather_infos(&self) -> Infos {
        let my_tree0_len = self
            .board
            .my_trees()
            .iter()
            .filter(|t| t.size == 0)
            .collect_vec()
            .len();
        let my_tree1_len = self
            .board
            .my_trees()
            .iter()
            .filter(|t| t.size == 1)
            .collect_vec()
            .len();
        let my_tree2_len = self
            .board
            .my_trees()
            .iter()
            .filter(|t| t.size == 2)
            .collect_vec()
            .len();
        let my_tree3_len = self
            .board
            .my_trees()
            .iter()
            .filter(|t| t.size == 3)
            .collect_vec()
            .len();

        let seed_cost = my_tree0_len;
        let grow1_cost = 1 + my_tree1_len;
        let grow2_cost = 3 + my_tree2_len;
        let grow3_cost = 7 + my_tree3_len;

        Infos {
            my_tree0_len,
            my_tree1_len,
            my_tree2_len,
            my_tree3_len,
            seed_cost,
            grow1_cost,
            grow2_cost,
            grow3_cost,
            opponent_trees3_count: self
                .board
                .opponent_trees()
                .iter()
                .filter(|t| t.size == 3)
                .collect_vec()
                .len(),
        }
    }

    // do we seed ? 100 yes, 0 no
    fn need_seed(&self, infos: &Infos) -> bool {
        // don't seed the 4 last days
        if self.turn + 5 > self.total_turn {
            return false;
        }

        // keep 1/3 seeds. don't seed if no tree >=2

        let trees = infos.my_tree3_len + infos.my_tree2_len;
        let seeds = infos.my_tree0_len;
        if trees < 2 {
            false
        } else if seeds * 3 < trees {
            true
        } else {
            false
        }
    }

    fn is_far_seed(&self, action: &Action) -> bool {
        match &action {
            Action::SEED(from, to) => !self.board.get_square(*from).is_neighbour(*to as i16),
            _ => false,
        }
    }

    fn get_seed_score(&self, position: usize) -> i16 {
        let square = self.board.get_square(position);
        let richness = square.richness as i16;
        let neighbours = square
            .neighbours
            .iter()
            .filter(|&i| i > &0)
            .map(|i| *i as usize)
            .collect_vec();
        let mut score = 100i16;
        // les voisins
        for t in self
            .board
            .trees
            .iter()
            .filter(|t| neighbours.contains(&t.square_idx))
        {
            if t.owner_idx == self.board.me.index {
                score = score - 3 * (t.size as i16 + 1);
                // malus for alignment
                let tree_square = self.board.get_square(t.square_idx);
                if square.is_aligned(tree_square.coord) {
                    let dist = square.distance(tree_square.coord) as i16;
                    score = score - (3 - dist);
                }
            } else {
                score = score + 2 * (3 - t.size as i16)
            }
        }

        score + richness
    }

    fn compare_seed_pos(&self, action1: &Action, action2: &Action) -> Ordering {
        if let (Action::SEED(_, t1), Action::SEED(_, t2)) = (action1, action2) {
            self.get_seed_score(*t2).cmp(&self.get_seed_score(*t1))
        } else {
            Ordering::Equal
        }
    }

    fn need_grow(&self) -> bool {
        true
    }

    fn compare_growth(&self, action1: &Action, action2: &Action) -> Ordering {
        if let (Action::GROW(t1), Action::GROW(t2)) = (action1, action2) {
            self.board
                .get_tree(*t2)
                .unwrap()
                .size
                .cmp(&self.board.get_tree(*t1).unwrap().size)
        } else {
            Ordering::Equal
        }
    }

    fn surrounded_trees_to_chop(&self, action: &Action) -> bool {
        if let Action::COMPLETE(t) = action {
            let t_square = self.board.get_tree(*t).unwrap().square_idx;
            let square = self.board.get_square(t_square);
            let mut res = true;
            for &n in square.neighbours.iter() {
                if n >= 0 {
                    res = res && self.board.has_tree(n as usize)
                }
            }
            return res;
        }
        return false;
    }

    fn surrounding_trees(&self, action: &Action) -> usize {
        let mut res = 0;
        if let Action::COMPLETE(t) = action {
            let t_square = self.board.get_tree(*t).unwrap().square_idx;
            let square = self.board.get_square(t_square);

            for &n in square.neighbours.iter() {
                if n >= 0 && self.board.has_tree(n as usize) {
                    res = res + 1
                }
            }
        }
        return res;
    }

    fn need_chop(&self, infos: &Infos) -> bool {
        if self.turn * 2 < self.total_turn {
            // don't chop in first half
            return false;
        }
        if self.turn * 4 > self.total_turn * 3 {
            // chop at the end of game (3/4)
            return true;
        }
        if infos.my_tree3_len > infos.opponent_trees3_count + 1 {
            // keep more trees then opponent
            return true;
        }

        return false;
    }

    fn compare_chop(&self, action1: &Action, action2: &Action) -> Ordering {
        let c = self
            .surrounding_trees(action2)
            .cmp(&self.surrounding_trees(action1));
        if c == Ordering::Equal {
            // order by square richness
            return c;
        } else {
            return c;
        }
    }

    fn best_move(&self) -> Action {
        let grows = self
            .moves
            .iter()
            .filter(|&a| match *a {
                Action::GROW(_) => true,
                _ => false,
            })
            .collect_vec();
        let seeds = self
            .moves
            .iter()
            .filter(|&a| match *a {
                Action::SEED(_, _) => true,
                _ => false,
            })
            .collect_vec();
        let completes = self
            .moves
            .iter()
            .filter(|&a| match *a {
                Action::COMPLETE(_) => true,
                _ => false,
            })
            .collect_vec();
        let infos = self.gather_infos();

        let mut seeds_candidates: Vec<&Action> = seeds
            .into_iter()
            .filter(|&action| self.is_far_seed(action))
            .collect_vec();
        seeds_candidates.sort_unstable_by(|&a1, &a2| self.compare_seed_pos(a1, a2));

        let mut grow_candidates = grows.into_iter().filter(|&action| true).collect_vec();
        grow_candidates.sort_unstable_by(|&a1, &a2| self.compare_growth(a1, a2));

        let mut completes_candidates = completes.into_iter().filter(|&action| true).collect_vec();
        completes_candidates.sort_unstable_by(|&a1, &a2| self.compare_chop(a1, a2));

        // chop surrounded
        let mut surrounded = completes_candidates
            .iter()
            .filter(|&action| self.surrounded_trees_to_chop(action))
            .map(|&a| a)
            .collect_vec();
        if !surrounded.is_empty() {
            *surrounded[0]
        } else if self.need_chop(&infos) && !completes_candidates.is_empty() {
            *completes_candidates[0]
        } else if self.need_seed(&infos) && !seeds_candidates.is_empty() {
            // seed !

            *seeds_candidates[0]
        } else if self.need_grow() && !grow_candidates.is_empty() {
            *grow_candidates[0]
        } else {
            Action::WAIT
        }
    }
}

#[cfg(test)]
mod tests {

    use crate::*;

    #[test]
    fn parse_action() {
        assert_eq!(Action::from(String::from("WAIT")), Action::WAIT);
        assert_eq!(Action::from(String::from("GROW 3")), Action::GROW(3));
        assert_eq!(Action::from(String::from("SEED 5 4")), Action::SEED(5, 4));
        assert_eq!(
            Action::from(String::from("COMPLETE 9")),
            Action::COMPLETE(9)
        );
    }
}
